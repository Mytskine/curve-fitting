Levenberg-Marquardt
===================

The algorithm is a classical way to solve non-linear least squares problems.
See [Wikipedia](https://en.wikipedia.org/wiki/Levenberg–Marquardt_algorithm) for a high level description,
and [Methods for non-linear least squares problems](https://www2.imm.dtu.dk/pubdb/edoc/imm3215.pdf) (2004, K. Madsen, H.B. Nielsen, O. Tingleff) (MNT2004)
for a precise description.

The following sections will describe implementation details which are not in that last document.

Damping
-------

Only the initial damping is configurable.

The damping is updated by multiplying or dividing by constant factors, as suggested by Marquardt.
Our hardcoded values were obtained after experimenting on our test set.
It differs from his (2, 3) choice.
```
if (ρ > 0.1) {
    λ *= upFactor
} else {
    λ /= downFactor
}
```

Other strategies were experimented, such as the one proposed by (MNT2004),
but the results were noticeably worse.
```
if (ρ > 0.1) {
    λ *= max(1/3, 1 - (2ρ-1)^3)
    v = 2
} else {
    λ /= v
    v *= 2
}
```

Convergence
-----------

The various stopping condition use hardcoded values in each case :
- convergence in gradient,
- convergence in parameters,
- convergence in error (reduced χ²),
- divergence (damping out of bounds).

Estimation of the Jacobian
--------------------------

There is presently no way to give an exact derivative of the model function.

The Jacobian of the model function is approximated with finite differences.
Most of the time, our implementation uses the faster forward differences,
but when the variations on the parameters become small,
it switches to the slower central differences.
