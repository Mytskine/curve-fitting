interface Options {
    damping: number;
    parameterDelta: Float64Array;
    parameter: {
        initial: Float64Array;
        max: null | Float64Array;
        min: null | Float64Array;
    };
    weights: Float64Array;
    convergence: {
        gradient: number;
        iterations: number;
        parameter: number;
        residual: number;
    }
}

function completeOptions(o: Partial<LM.Options>, input: LM.InputData): Options {
    if (!Array.isArray(o?.parameters?.initial) || o?.parameters?.initial.length === 0) {
        throw new Error("You must pass an array of numbers into the option 'parameters.initial'.");
    }
    const parameterSize = o.parameters.initial.length;

    let parameterDelta;
    if (Array.isArray(o?.parametersDelta) && o.parametersDelta.length > 0) {
        if (o.parametersDelta.length === parameterSize) {
            parameterDelta = new Float64Array(o.parametersDelta);
        } else if (o.parametersDelta.length === 1 && typeof o.parametersDelta[0] === 'number') {
            parameterDelta = new Float64Array(parameterSize);
            parameterDelta.fill(o.parametersDelta[0]);
        } else {
            throw new Error("Incorrect value for parametersDelta");
        }
    } else if (!o?.parametersDelta) {
        const averageInterval = (input.x[input.x.length - 1] - input.x[0]) / input.x.length;
        if (averageInterval < 0) {
            throw new Error("Input data x must be sorted by ascending values.");
        }
        parameterDelta = new Float64Array(parameterSize);
        parameterDelta.fill(averageInterval < 1e-6 ? averageInterval / 100 : 1e-8);
    } else {
        throw new Error("Incorrect value for parametersDelta");
    }

    let weights: Float64Array;
    if (o.weights) {
        weights = new Float64Array(o.weights);
    } else {
        weights = new Float64Array(input.x.length).fill(1);
    }

    let maxIterations = 500;
    if (typeof o?.maxIterations === 'number' && o.maxIterations > 0) {
        maxIterations = o.maxIterations;
    } else if (parameterSize > 1) {
        maxIterations = 500 * parameterSize;
    }

    return {
        convergence: {
            gradient: o?.convergence?.gradient || 1e-2,
            iterations: maxIterations,
            parameter: o?.convergence?.parameter || 1e-6,
            residual: o?.convergence?.residual || 1e-12,
        },
        damping: (typeof o?.damping === 'number' && o.damping > 0 ? o.damping : 2),
        parameterDelta,
        parameter: {
            initial: new Float64Array(o.parameters.initial),
            max: o.parameters?.max ? new Float64Array(o.parameters.max) : null,
            min: o.parameters?.min ? new Float64Array(o.parameters.min) : null,
        },
        weights,
    };
}

export {
    Options,
    completeOptions,
}
