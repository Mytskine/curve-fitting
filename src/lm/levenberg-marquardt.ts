import * as LA from '../LinearAlgebra'
import * as LM from '../types'
import ComputeNext from './compute-next';
import { completeOptions } from './options';

const epsilonStep = 1e-4;

// Damping
const dampingStepUp = 8; // reject step
const dampingStepDown = 9; // accept step
const dampingMin = 1e-20;
const dampingMax = 1e15;

/**
 * Find the optimal parameter array so that the model function fits the data points.
 *
 * More precisely, this function uses the Levenberg-Marquardt algorithm
 * to find a local optimum of the first argument (parameter array) of the model function,
 * so that the least-squares residual to the data is minimal (best fit).
 *
 * I.e. for (x_i, y_i) given by the calling parameter `input`, and f by `fn`,
 * find p such that : leastsquares(f(p)(x_i), y_i) is minimal.
 */
function levenbergMarquardt(input: LM.InputData, fn: LM.ModelFunction, o: Partial<LM.Options>): LM.Result {
    const options = completeOptions(o, input);
    const parameterSize = options.parameter.initial.length;
    const numPoints = input.x.length;
    if (input.x.length !== input.y.length) {
        throw new Error("The arrays input.x and input.y have different sizes.");
    }
    if (input.x.length <= parameterSize) {
        throw new Error("Fitting requires more input points than parameters in the model function.");
    }

    let parameter = options.parameter.initial.slice(0);
    let parameterOld = parameter.slice(0);

    let yOld = new Float64Array(numPoints); // i.e. modelValues : eval of `modelFunction` for `input.x`
    let y = new Float64Array(input.x.map(fn(Array.from(parameter))));

    const computeNext = new ComputeNext(input, fn, parameterSize, options.weights);

    const jacobian = new LA.Matrix(numPoints, parameterSize); // AKA J
    const JtWJ = new LA.Matrix(parameterSize, parameterSize); // AKA J^T * W * J
    const JtWdy = new Float64Array(parameterSize);
    computeNext.updateJacobian(jacobian, parameter, y);
    computeNext.updateJtWJ(JtWJ, jacobian);
    computeNext.updateJtWdy(JtWdy, jacobian, y);

    let chi2 = computeNext.computeChi2(y);
    let chi2Old = 0;

    let lambda = options.damping;
    let convergence: ""|"failed"|"gradient"|"iterations"|"parameter"|"residual" = "";

    let iteration = 0;
    while (convergence === "") {
        iteration++;
        if (iteration > options.convergence.iterations) {
            convergence = "iterations";
            break;
        }

        // Change parameter
        const h = computeNext.computeParametersDelta(lambda, JtWJ, JtWdy)
        parameterOld = parameter.slice(0);
        for (let i = 0; i < parameterSize; i++) {
            parameter[i] += h[i];
            // Apply parameter bounds
            if (options.parameter.min && parameter[i] < options.parameter.min[i]) {
                parameter[i] = options.parameter.min[i];
            }
            if (options.parameter.max && parameter[i] > options.parameter.max[i]) {
                parameter[i] = options.parameter.max[i];
            }
        }

        // Evaluate the model function
        yOld = y.slice(0);
        y = new Float64Array(input.x.map(fn(Array.from(parameter))));

        // Compute the error
        chi2Old = chi2;
        chi2 = computeNext.computeChi2(y);

        // Is this an successful step?
        const rho = (chi2Old - chi2) / computeNext.computeRhoDenominator(h, lambda, JtWdy);
        //console.log("H0", h[0], "CHI2", chi2, "rho", rho, "lambda", lambda);
        if (rho > epsilonStep) {
            //console.log("P", parameter);
            // The new parameter is significantly better.

            computeNext.updateJacobian(jacobian, parameter, y);
            computeNext.updateJtWJ(JtWJ, jacobian);
            computeNext.updateJtWdy(JtWdy, jacobian, y);
        
            // Decrease lambda ⇒ toward a Gauss-Newton method
            lambda = Math.max(lambda / dampingStepDown, dampingMin);
        } else {
            // Revert to the old parameter.
            parameter = parameterOld.slice(0);
            y = yOld.slice(0);
            chi2 = chi2Old;

            // Increase lambda ⇒ toward a gradient descent
            if (lambda >= dampingMax) {
                // Convergence failed or too slow
                convergence = "failed";
                break;
            } else {
                lambda = lambda * dampingStepUp;
            }

            // Do not test for convergence, because this step failed.
            continue;
        }

        // Test for convergence
        if (iteration <= 2) {
            continue;
        }
        if (LA.normInf(JtWJ) < options.convergence.gradient) {
            // Convergence in gradient
            convergence = "gradient";
        } else if (chi2 / input.x.length < options.convergence.residual) {
            // Convergence in least-squares residual (reduced chi2)
            convergence = "residual";
        } else {
            let hMax = 0;
            for (let i = 0; i < parameterSize; i++) {
                const pAbs = Math.abs(parameter[i]);
                if (pAbs > 1) {
                    hMax = Math.max(hMax, Math.abs(h[i]) / pAbs);
                } else {
                    hMax = Math.max(hMax, Math.abs(h[i]))
                }
            }
            if (hMax < options.convergence.parameter) {
                // Convergence in parameter
                convergence = "parameter";
            }
        }
    }

    return {
        convergence,
        error: chi2,
        iterations: iteration,
        parameters: Array.from(parameter),
    };
}

export default levenbergMarquardt
