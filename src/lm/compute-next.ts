import * as LA from '../LinearAlgebra'
import * as LM from '../types'
import { Options } from './options'

const epsilonDifferences = 1e-4;

export default class ComputeNext {
    #centralDifferences
    #input: LM.InputData
    #modelFunction: LM.ModelFunction
    #weights: Float64Array
    /**
     * A vector that scales the parameter (ellipsoidal trust region)
     */
    #dtd: Float64Array
    constructor(input: LM.InputData, modelFunction: LM.ModelFunction, paramSize: number, weights: Float64Array) {
        this.#input = input;
        this.#modelFunction = modelFunction;
        this.#weights = weights;

        this.#centralDifferences = new Array(paramSize);
        this.#centralDifferences.fill(false);

        this.#dtd = new Float64Array(paramSize);
        this.#dtd.fill(1);
    }
    /**
     * Return the vector H that solves : (J^T W J + λ I) H = J^T W (Y - Ŷ).
     */
    computeParametersDelta(lambda: number, JtWJ: LA.Matrix, JtWdy: Float64Array): Float64Array {
        // Add λI to J^T W J
        for (let i = 0; i < JtWJ.rows; i++) {
            JtWJ.values[i * JtWJ.columns + i] += lambda * this.#dtd[i];
        }
        // Solve the linear equations.
        const Y = new LA.Matrix(JtWdy.length, 1, JtWdy);
        const h = LA.solve(JtWJ, Y).values;
        // When the Δp is small, we switch to a more precise estimation of the Jacobian.
        for (let i = 0; i < h.length; i++) {
            this.#centralDifferences[i] = (h[i] < epsilonDifferences);
        }

        return h;
    }
    computeChi2(y: Float64Array): number {
        let sum = 0;
        for (let i = 0; i < this.#input.y.length; i++) {
            sum += Math.pow(this.#input.y[i] - y[i], 2) * this.#weights[i];
        }
        return sum;
    }
    computeRhoDenominator(h: Float64Array, lambda: number, JtWdy: Float64Array): number {
        // |dotproduct(h, λ h + JtWdy)|
        let sum = 0;
        for (let i = 0; i < h.length; i++) {
            sum += h[i] * (lambda * h[i] + JtWdy[i]);
        }
        return Math.abs(sum);
    }
    updateJacobian(J: LA.Matrix, params: Float64Array, y: Float64Array): void {
        const deltas = this.#updateDifferenceScales(y);
        const x = Array.from(this.#input.x);
        const pRight = Array.from(params);
        const pLeft = Array.from(params);
        for (let j = 0; j < J.columns; j++) {
            for (let i = 0; i < J.rows; i++) {
                const delta = deltas[i];
                pRight[j] += delta;
                pLeft[j] -= delta;
                const yRight = this.#modelFunction(pRight).call(null, x[i]);
                if (this.#centralDifferences) {
                    // Central differences (slower, but the precision is required here)
                    const yLeft = this.#modelFunction(pLeft).call(null, x[i]);
                    J.values[i * J.columns + j] = (yRight - yLeft) / (2 * delta);
                } else {
                    // Forward differences (faster)
                    J.values[i * J.columns + j] = (yRight - y[i]) / delta;
                }
                pRight[j] = params[j];
                pLeft[j] = params[j];
            }
        }
    }
    updateJtWJ(JtWJ: LA.Matrix, jacobian: LA.Matrix): void {
        for (let i = 0; i < JtWJ.rows; i++) { // 0 .. numParameters-1
            for (let j = 0; j < JtWJ.columns; j++) { // 0 .. numParameters-1
                // If ignoring weights: JtWJ[i,j] = dotproduct(col i of J, col j of J)
                let sum = 0;
                for (let k = 0; k < jacobian.rows; k++) {
                    sum += jacobian.values[k * jacobian.columns + i] * this.#weights[k] * jacobian.values[k * jacobian.columns + j];
                }
                JtWJ.values[i * JtWJ.columns + j] = sum;
            }
        }
    }
    updateJtWdy(JtWdy: Float64Array, jacobian: LA.Matrix, y: Float64Array): void {
        for (let j = 0; j < JtWdy.length; j++) {
            // If ignoring weights: JtWdy[j] = dotproduct(col j of J, input.y - y)
            let sum = 0;
            for (let k = 0; k < jacobian.rows; k++) {
                sum += jacobian.values[k * jacobian.columns + j] * this.#weights[k] * (this.#input.y[k] - y[k]);
            }
            JtWdy[j] = sum;
        }
    }
    /**
     * Find the optimal precision for finite differences.
     *
     * For a double float, the optimal h in the central difference
     * (f(x+h) - f(x-h)) / (2*h) is cubicRoot(M * precision / 2),
     * where precision = 2^-52 ≈ 10^-16 and M = f(h) / f''(x).
     */
    #updateDifferenceScales(y: Float64Array): Float64Array {
        const deltas = new Float64Array(y.length);
        const centralScale = 5e-6;
        const forwardScale = 1e-9;
        for (let i = 0; i < y.length; i++) {
            deltas[i] = (this.#centralDifferences[i] ? centralScale : forwardScale);
        }
        return deltas;
    }
};
