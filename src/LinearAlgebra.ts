class Matrix {
    rows: number;
    columns: number;
    values: Float64Array;

    constructor(rows: number, columns: number, values?: Float64Array | number[]) {
        this.rows = rows;
        this.columns = columns;
        if (values) {
            if (values instanceof Float64Array) {
                this.values = values;
            } else {
                this.values = new Float64Array(values);
            }
        } else {
            this.values = new Float64Array(rows * columns);
            this.values.fill(0);
        }
    }
}

/**
 * L inf norm, i.e. normInf(m) = max(|m_ij|)
 */
function normInf(m: Matrix): number {
    let max = 0;
    for (const x of m.values) {
        max = Math.max(max, Math.abs(x));
    }
    return max;
}

/**
 * L2-norm, i.e. norm2(m) = sqrt(Σ m_ij²)
 */
function norm2(m: Matrix): number {
    let sum = 0;
    for (const x of m.values) {
        sum += Math.pow(x, 2);
    }
    return Math.sqrt(sum);
}

const almostZero = 1e-20;

/**
 * Return the matrix X such that M X = Y.
 *
 * Will throw an error if M is not inversible.
 */
function solve(m: Matrix, y: Matrix): Matrix {
    // Concat the matrix M and the matrix Y.
    if (m.rows !== y.rows) {
        throw new Error("Cannot solve because of wrong dimensions.");
    }
    const a = new Float64Array(m.rows * (y.columns + m.columns));
    let p = 0;
    let py = 0;
    for (let im = 0; im < m.rows * m.columns; im++) {
        a[p] = m.values[im];
        p++;
        if (im % m.columns === m.columns - 1) {
            for (let j = 0; j < y.columns; j++) {
                a[p] = y.values[py];
                p++;
                py++;
            }
        }
    }

    // Gauss-Jordan elimination
    const numRows = m.rows;
    const numColumns = m.columns + y.columns;
    const size = m.rows * m.columns;
    for (let i = 0; i < numRows; i++) {
        const pivot = i * (numColumns + 1);
        // Ensure that the pivot cell is not zero.
        if (Math.abs(a[pivot]) < almostZero) {
            let swapped = false;
            for (let j = pivot + numColumns; j < size; j += numColumns) {
                if (Math.abs(a[j]) > almostZero) {
                    // Swap the rows
                    const row = a.slice(j, j + numColumns - i);
                    for (let k = 0; k < numColumns - i; k++) {
                        a[j + k] = a[pivot + k];
                        a[pivot + k] = row[k];
                    }
                    swapped = true;
                    break;
                }
            }
            if (!swapped) {
                console.error(a);
                throw new Error("No solution.");
            }
        }

        // 1 on the diagonal
        const c = a[pivot];
        a[pivot] = 1;
        for (let k = 1; k < numColumns - i; k++) {
            a[pivot + k] /= c;
        }

        // Substract this row to others
        for (let r = 0; r < numRows; r++) {
            if (r === i || Math.abs(a[r * numColumns + i]) < almostZero) {
                continue;
            }
            const sub = a[r * numColumns + i];
            a[r * numColumns + i] = 0;
            for (let k = 1; k < numColumns - i; k++) {
                a[r * numColumns + i + k] -= sub * a[pivot + k];
            }
        }
    }

    // x = M^-1 Y
    const x = new Matrix(m.columns, y.columns);
    for (let i = 0; i < x.rows; i++) {
        for (let j = 0; j < x.columns; j++) {
            x.values[i * x.columns + j] = a[i * numColumns + m.columns + j];
        }
    }
    return x;
}

export {
    Matrix,
    norm2,
    normInf,
    solve,
}
