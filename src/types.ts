// public types
type Fitting = (d: InputData, fn: ModelFunction, o?: Partial<Options>) => Result;

/**
 * Function that will fit into the points.
 */
type ModelFunction = (parameters: number[]) => (x: number) => number;

/**
 * Coordinates of the points to fit.
 */
interface InputData {
    x: number[] | Float64Array;
    y: number[] | Float64Array;
}

interface Options {
    /**
     * Optional configuration of the convergence conditions.
     */
    convergence?: {
        /**
         * The algorithm will stop if the 2-norm of the Jacobian is smaller than this.
         * Default value: 1e-3
         */
        gradient?: number;

        /**
         * The maximum number of iterations before halting.
         * Default value: 400 * parameters.length
         */
        iterations?: number;

        /**
         * The algorithm will stop if the last sucessful step made a small parameter change.
         * For each component of the parameter, the rate of change is computed,
         * then the max rate is compared with this stoping criteria.
         * For instance, 1e-2 means that the last step modified each parameter component by less than a percent.
         * Default value: 1e-3
         */
        parameter?: number;

        /**
         * The algorithm will stop if the (sum of the squared residuals)/(number of points) is smaller than this.
         * The division is required so that adding input points does not slow the convergence.
         * Default value: 1e-10
         */
        residual?: number;
    }

    /**
     * The Levenberg-Marquardt initial lambda parameter, AKA λ₀.
     * It's usually recommended to set an initial value > 1.
     * Default value: 2
     */
    damping?: number;

    /**
     * The step size to approximate the jacobian matrix with finite differences.
     * If set too large, the estimation of the derivation (relative to a parameter) will be imprecise.
     * If set too low, you risk numerical unstability.
     * The format is either a single number, or a number for each parameter.
     * Default value: [min (1e-8, (xmax -xmin) / x.length / 100)]
     */
    parametersDelta?: number[];

    parameters: {
        /**
         * REQUIRED.
         * Initial guess for the parameters of the model function.
         */
        initial: number[];

        /**
         * Optional. Maximum values for each of the parameters.
         */
        max?: number[];

        /**
         * Optional. Minimum values for each of the parameters.
         */
        min?: number[];
    }

    /**
     * Weighting vector: an array either empty, or with the same length of the input data.
     * Usually, the weight in x_i is the square inverse of the uncertainty at x_i.
     * E.g.  [y_i^-2]  for an uncertainty proportional to the value at x_i.
     * The weights modify the residuals, so they influence the solution and a stopping criteria.
     * Default value: [] (same weight 1 on every point)
     */
    weights?: number[];
}

interface Result {
    /**
     * The condition that stopped the algorithm.
     */
    convergence: "failed"|"gradient"|"iterations"|"parameter"|"residual";

    /**
     * The weighted least-squares error, AKA χ² = Σ (y_i - f_p(x_i))² * w_i
     */
    error: number;

    /**
     * Nυmber of iterations of the LM algorithm.
     */
    iterations: number;

    /**
     * Final parameters of the model function.
     */
    parameters: number[];
}

export {
    ModelFunction,
    InputData,
    Result,
    Options,
}
