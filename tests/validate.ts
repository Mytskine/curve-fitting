import { argv } from 'node:process'

import testLinearAlgebra from './series/linearalgebra'
import testExact from './series/exact'
import testNist from './series/nist'
import testRealworld from './series/realworld'

const only = argv.length > 2 ? argv[2] : "";

if (only === "") {
    testLinearAlgebra();
    testExact();
}
testNist(only);
testRealworld(only);
