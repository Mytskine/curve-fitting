const inverseData = {
    x: [
        100, 200, 300, 400, 500, 600, 700, 800, 900, 1000
    ],
    y: [
        1557.69230769231, 786.764705882353, 517.441860465116, 401.041666666667 , 323.529411764706, 273.584905660377, 231.818181818182, 202.702702702703, 174.107142857143, 163.716814159292
    ],
};

const expData = {
    x: [
        20, 40, 60, 80, 100, 120, 180, 240, 300, 360, 420, 480, 540, 600
    ],
    y: [
        1025, 830, 671, 532, 439, 358, 208, 110, 84, 70, 46, 44, 41, 40.4
    ],
};


export default [
    {
        name: 'simple inverse',
        modelFunction:
            ([b1]) =>
                (x) =>
                    10 * b1 / x,
        data: inverseData,
        expected: {
            error: 623.577,
            parameters: [15668.739616374163],
        },
        options: {
            parameters: {
                initial: [1],
            },
        },
    },
    {
        name: 'unstable inverse',
        modelFunction:
            ([b1]) =>
                (x) =>
                    10 / (b1 * x),
        data: inverseData,
        expected: {
            error: 623.577,
            parameters: [6.38213e-5],
        },
        options: {
            parameters: {
                initial: [2e-4],
            },
        },
    },
    {
        name: 'inverse+weights',
        modelFunction:
            ([b1]) =>
                (x) =>
                    10 * b1 / x,
        data: inverseData,
        expected: {
            error: 0.440001,
            parameters: [15947.6],
        },
        options: {
            convergence: {
                gradient: 1e-10, // This test requires that we continue even when the gradient seems too small.
            },
            parameters: {
                initial: [1],
            },
            weights: [ 100.0, 90.0, 80.0, 70.0, 60.0, 50.0, 40.0, 30.0, 20.0, 10.0 ].map(e => 1 / e**2),
        },
    },
    {
        name: 'exp',
        modelFunction:
            ([b1, b2, b3]) =>
                (x) =>
                    b1 * Math.exp(-x / b2) + b3,
        data: expData,
        expected: {
            error: 390.508,
            parameters: [1238.3, 88.3088, 39.4791],
        },
        options: {
            parameters: {
                initial: [1000, 10, 0],
            },
        },
    },
    {
        name: 'exp+weights',
        modelFunction:
            ([b1, b2, b3]) =>
                (x) =>
                    b1 * Math.exp(-x / b2) + b3,
        data: expData,
        expected: {
            error: 0.269062,
            parameters: [1236.99, 88.6015, 38.8903],
        },
        options: {
            parameters: {
                initial: [1000, 10, 0],
            },
            weights: [ 75.0, 70.0, 65.0, 60.0, 55.0, 50.0, 45.0, 40.0, 35.0, 30.0, 25.0, 20.0, 15.0, 10.0 ].map(e => 1 / e**2),
        },
    },
];
