import dataNist from './data/nist'
import dataRealworld from './data/realword'
import { levenbergMarquardt } from '../src/main'
import { argv } from 'node:process'
import chalk from 'chalk'
import { readFileSync } from 'node:fs'
import { performance } from 'node:perf_hooks'

if (argv.length < 3) {
    help();
} else if (!['csv', 'json', 'compare'].includes(argv[2])) {
    console.error(`Unknown action '${argv[2]}'`)
    help();
} else {
    run(argv[2], argv.slice(3));
}

function help() {
    console.log(`Usage:
    benchmark csv
    benchmark json
    benchmark compare <jsonFile>
`);
}

interface Result {
    name: string;
    convergence: string;
    error: number;
    iterations: number;
    duration: number;
}
type Results = {
    [index: string]: Result;
}

function run(action: string, extra: string[]) {
    const data = dataNist.concat(dataRealworld);

    const results: Results = {};
    for (const tcase of data) {
        const startTime = performance.now();
        const result = levenbergMarquardt(tcase.data, tcase.modelFunction, tcase.options);
        const duration = performance.now() - startTime;
        results[tcase.name] = {
            name: tcase.name,
            convergence: result.convergence,
            error: result.error,
            iterations: result.iterations,
            duration,
        };
    }

    if (action === 'csv') {
        console.log("Test name\tleast-squares\titerations\tuser time");
        for (const e of Object.entries(results)) {
            const name = e[0] as string;
            const r = e[1] as Result;
            console.log(
                "%s\t%s\t%s\t%s\t%s",
                name.padEnd(10),
                r.error.toExponential(2).padStart(13),
                r.iterations.toString().padStart(10),
                r.duration.toFixed(2).padStart(9),
                r.convergence
            );
        }
    } else if (action === 'json') {
        console.log(JSON.stringify(results, undefined, 4));
    } else if (action === 'compare') {
        if (!extra || extra?.length < 1) {
            console.error("The 'compare' action requires a path parameter.");
            return;
        }
        console.log(
            chalk.yellow('%s %s %s  %s'),
            "Test".padEnd(20, ' '),
            "Error".padStart(10, ' '),
            "Iterations".padStart(10, ' '),
            "Convergence",
        )
        const overall = {
            errors: 0,
            iterations: 0,
        };
        const base = JSON.parse(readFileSync(extra[0]));
        for (const e of Object.entries(base)) {
            const name = e[0] as string;
            if (!results.hasOwnProperty(name)) {
                console.warn("%s is not in the new tests.", name);
                continue;
            }
            const b = e[1] as Result;
            const r = results[name];
            console.log(
                '%s %s %s  %s',
                name.padEnd(20, ' '),
                formatNumber(computeRate(b.error, r.error)),
                formatNumber(computeRate(b.iterations, r.iterations)),
                r.convergence,
            );
            if (r.error < 0.99 * b.error) {
                overall.errors--;
            } else if (r.error > 1.01 * b.error) {
                overall.errors++;
            }
            overall.iterations += r.iterations - b.iterations;
        }
        console.log(
            '%s %s %s',
            "OVERALL".padEnd(20, ' '),
            overall.errors.toString().padStart(10),
            overall.iterations.toString().padStart(10),
        );
    }
}

function formatNumber(x: [number, string], size: number = 8, incIsGood: boolean = false): string {
    let n = x[0];
    let str = '';
    const abs = Math.abs(n);
    if (x[1] === 'relative' && abs < 1) {
        str = '0';
    } else if (abs < 1e-1 || abs > 1e4) {
        str = n.toExponential(1);
    } else if (abs < 2) {
        str = n.toPrecision(1);
    } else {
        str = Math.round(n).toFixed(0);
    }
    if (x[1] === 'relative') {
        str += ' %';
    }
    if (size > 0) {
        str = str.padStart(10, ' ');
    }

    if (abs < 1) {
        return str;
    }
    if (!incIsGood) {
        n *= -1;
    }
    if (n < 0) {
        return chalk.bgRed(str);
    }
    return chalk.black(chalk.bgGreen(str));
}

function computeRate(base: number, update: number): [number, string] {
    let rate = 0;
    if (base > 0) {
        return [100 * (update - base) / base, 'relative'];
    }
    return [update, 'absolute'];
}
