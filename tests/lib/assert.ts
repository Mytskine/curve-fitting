type AssertionError = {
    assertion: string;
    message: string;
};

const assert = (b: boolean, err: AssertionError) => {
    if (!b) {
        const error = new Error(`    in the assertion ${err.assertion}()\n${err.message}`);
        error.name = "assertion";
        throw error;
    }
}

/**
 * Assert a number is nearly equal to the expeted value (default: ±1%).
 */
const nearFloat = (expected: number, got: number, precision?: number) => {
    if (precision === undefined || precision === 0) {
        precision = Math.max(1e-8, 1e-2 * Math.abs(expected));
    }
    assert(
        Math.abs(expected - got) < precision,
        {assertion: "nearFloat", message: `\tExpected: ${expected} ± ×${precision.toExponential(1)}\n\tReceived: ${got}`}
    );
};

/**
 * Assert the parameters have the same type and value.
 */
const same = (a: any, b: any) => assert(
    a === b,
    {assertion: "same", message: `Expected: ${a}\nReceived: ${b}\n`}
);

export default {
    nearFloat,
    same,
}
