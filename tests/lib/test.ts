type TestFunction = () => void;

function test(name: string, callable: TestFunction) {
    try {
        callable.call(this);
        console.log(`✓ ${name}`);
    } catch (err) {
        console.log(`× ${name}`);
        if (err.hasOwnProperty("name") && err.name === 'assertion') {
            console.error(`ERROR, the test '${name}' failed.`);
            renderError(err);
        } else {
            console.error("CRASH, error thrown while running a test");
            console.error(err);
        }
    }
}

function renderError(err: Error): void {
    console.warn(err.message);
    if (err.stack) {
        const caller = err.stack.match(/^ +at assert .+\n.+\n(.+)\n/m);
        if (caller) {
            console.warn(caller[1]);
        } else {
            console.warn(err.stack);
        }
    }
}

export default test;
