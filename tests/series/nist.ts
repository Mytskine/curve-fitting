import dataNist from '../data/nist'
import assert from '../lib/assert'
import { levenbergMarquardt } from '../../src/main'
import test from '../lib/test'

export default function(only: string) {
    for (const tcase of dataNist) {
        if (only !== "" && tcase.name !== only) {
            continue;
        }
        const results = levenbergMarquardt(tcase.data, tcase.modelFunction, tcase.options);
        test(`NIST ${tcase.name}`, () => {
            assert.nearFloat(tcase.expected.error, results.error);
        })
        //console.log("\tIterations", results.iterations, "error", results.error);
    }
}
