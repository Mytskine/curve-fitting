import dataRealworld from '../data/realword'
import assert from '../lib/assert'
import { levenbergMarquardt } from '../../src/main'
import test from '../lib/test'

export default function(only: string) {
    for (const tcase of dataRealworld) {
        if (only !== "" && tcase.name !== only) {
            continue;
        }
        const results = levenbergMarquardt(tcase.data, tcase.modelFunction, tcase.options);
        test(`RealWorld ${tcase.name}`, () => {
            assert.nearFloat(tcase.expected.error, results.error, 1e-2);
            for (let i = 0; i < tcase.expected.parameters.length; i++) {
                assert.nearFloat(tcase.expected.parameters[i], results.parameters[i], 0.1);
            }
        })
    }
}
